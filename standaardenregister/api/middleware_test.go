package api

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCheckProbeAccess(t *testing.T) {
	router := gin.New()
	router.Use(checkProbeAccess())

	// Add test path to router
	router.GET("/healthz", func(c *gin.Context) {
		c.Status(http.StatusOK)
	})

	t.Run("TrafficIsBlocked", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/healthz", nil)
		req.Header.Set("User-Agent", "some-other-user-agent")
		sut := httptest.NewRecorder()
		router.ServeHTTP(sut, req)

		assert.Equal(t, sut.Code, http.StatusForbidden)
	})

	t.Run("TrafficIsAllowed", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/healthz", nil)
		req.Header.Set("User-Agent", "kube-probe/1.20")
		sut := httptest.NewRecorder()
		router.ServeHTTP(sut, req)
		assert.Equal(t, sut.Code, http.StatusOK)
	})
}
