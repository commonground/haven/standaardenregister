package api

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"github.com/stretchr/testify/assert"
	"math/big"
	"os"
	"testing"
	"time"
)

func generateCACertificate() (*x509.Certificate, *rsa.PrivateKey, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}

	notBefore := time.Now()
	notAfter := notBefore.Add(time.Hour)

	serialNumber, _ := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Test CA"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
		BasicConstraintsValid: true,
		IsCA:                  true,
		MaxPathLen:            0,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, nil, err
	}

	cert, err := x509.ParseCertificate(derBytes)
	if err != nil {
		return nil, nil, err
	}

	return cert, priv, nil
}

func generateTestCertificateFromCa(caCert *x509.Certificate, caPrivKey *rsa.PrivateKey, isClient bool, org, serial string) (*tls.Certificate, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	notBefore := time.Now()
	notAfter := notBefore.Add(time.Hour)

	subject := pkix.Name{}

	if org != "" {
		subject.Organization = []string{org}
	}

	if serial != "" {
		subject.SerialNumber = serial
	}

	serialNumber, _ := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject:      subject,
		NotBefore:    notBefore,
		NotAfter:     notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		BasicConstraintsValid: true,
	}

	if isClient {
		template.ExtKeyUsage = []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth}
	} else {
		template.ExtKeyUsage = []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth}
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, caCert, &priv.PublicKey, caPrivKey)
	if err != nil {
		return nil, err
	}

	cert, err := x509.ParseCertificate(derBytes)
	if err != nil {
		return nil, err
	}

	tlsCert := tls.Certificate{
		Certificate: [][]byte{derBytes, caCert.Raw}, // Include the CA's certificate
		PrivateKey:  priv,
		Leaf:        cert,
	}

	return &tlsCert, nil
}

func generateTestCertificate(org, serial string) (*tls.Certificate, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	notBefore := time.Now()
	notAfter := notBefore.Add(time.Hour)

	subject := pkix.Name{}

	if org != "" {
		subject.Organization = []string{org}
	}

	if serial != "" {
		subject.SerialNumber = serial
	}

	serialNumber, _ := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject:      subject,
		NotBefore:    notBefore,
		NotAfter:     notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, err
	}

	cert, _ := x509.ParseCertificate(derBytes) // Manually parse the certificate

	tlsCert := tls.Certificate{
		Certificate: [][]byte{derBytes},
		PrivateKey:  priv,
		Leaf:        cert, // Set the Leaf field manually
	}

	return &tlsCert, nil
}

// generatePEMFiles generates corresponding PEM file content for CA and a certificate
func generatePEMFiles(ca *x509.Certificate, cert *tls.Certificate) ([]byte, []byte, []byte) {
	caPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "CERTIFICATE",
		Bytes: ca.Raw,
	})

	certPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "CERTIFICATE",
		Bytes: cert.Certificate[0],
	})

	keyPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(cert.PrivateKey.(*rsa.PrivateKey)),
	})

	return caPEM, certPEM, keyPEM
}

func TestExtractCerts(t *testing.T) {
	org := "testOrganisationPKIO"
	serial := "1234567890"

	t.Run("CertValid", func(t *testing.T) {
		validCert, err := generateTestCertificate(org, serial)
		assert.Nil(t, err)

		tlsInfo := &tls.ConnectionState{
			PeerCertificates: []*x509.Certificate{validCert.Leaf},
		}

		certModel, err := extractClientCertificate(tlsInfo)
		assert.Nil(t, err)
		assert.Equal(t, org, certModel.Organisation)
		assert.Equal(t, serial, certModel.OIN)
	})

	t.Run("CertInvalid", func(t *testing.T) {
		tlsInfo := &tls.ConnectionState{
			PeerCertificates: []*x509.Certificate{},
		}

		certModel, err := extractClientCertificate(tlsInfo)
		assert.NotNil(t, err)
		assert.Nil(t, certModel)
	})

	t.Run("CertNil", func(t *testing.T) {
		tlsInfo := &tls.ConnectionState{
			PeerCertificates: nil,
		}

		certModel, err := extractClientCertificate(tlsInfo)
		assert.NotNil(t, err)
		assert.Nil(t, certModel)
	})

	t.Run("OrgEmpty", func(t *testing.T) {
		cert, err := generateTestCertificate("", serial)
		assert.Nil(t, err)

		tlsInfo := &tls.ConnectionState{
			PeerCertificates: []*x509.Certificate{cert.Leaf},
		}

		certModel, err := extractClientCertificate(tlsInfo)
		assert.NotNil(t, err)
		assert.Nil(t, certModel)
	})

	t.Run("SerialEmpty", func(t *testing.T) {
		cert, err := generateTestCertificate(org, "")
		assert.Nil(t, err)

		tlsInfo := &tls.ConnectionState{
			PeerCertificates: []*x509.Certificate{cert.Leaf},
		}

		certModel, err := extractClientCertificate(tlsInfo)
		assert.NotNil(t, err)
		assert.Nil(t, certModel)
	})
}

func TestCreateTlsConfigFromPaths(t *testing.T) {
	caCert, caPrivKey, err := generateCACertificate()
	assert.Nil(t, err)

	serverCert, err := generateTestCertificateFromCa(caCert, caPrivKey, false, "Test Org", "Test Serial")
	assert.Nil(t, err)

	caPEM, serverCertPEM, serverKeyPEM := generatePEMFiles(caCert, serverCert)

	assert.Nil(t, err)

	t.Run("HappyPath", func(t *testing.T) {
		os.Setenv(ROOTCA, string(caPEM))
		os.Setenv(MTLSCERT, string(serverCertPEM))
		os.Setenv(MTLSKEY, string(serverKeyPEM))
		os.Setenv(TLSKEY, string(serverKeyPEM))
		os.Setenv(TLSCERT, string(serverCertPEM))
		config, err := CreateTlsConfig()
		assert.Nil(t, err)
		assert.NotNil(t, config)
		os.Unsetenv(ROOTCA)
		os.Unsetenv(MTLSCERT)
		os.Unsetenv(MTLSKEY)
		os.Unsetenv(TLSKEY)
		os.Unsetenv(TLSCERT)
	})

	t.Run("NoValidPaths", func(t *testing.T) {
		os.Setenv(ROOTCA, "")
		os.Setenv(MTLSCERT, "")
		os.Setenv(MTLSKEY, "")
		os.Setenv(TLSKEY, string(serverKeyPEM))
		os.Setenv(TLSCERT, string(serverCertPEM))
		config, err := CreateTlsConfig()
		assert.NotNil(t, err)
		assert.Nil(t, config)
		os.Unsetenv(ROOTCA)
		os.Unsetenv(MTLSCERT)
		os.Unsetenv(MTLSKEY)
		os.Unsetenv(TLSKEY)
		os.Unsetenv(TLSCERT)
	})

	t.Run("CaValidKeyInvalid", func(t *testing.T) {
		os.Setenv(ROOTCA, string(caPEM))
		os.Setenv(MTLSCERT, "")
		os.Setenv(MTLSKEY, "")
		os.Setenv(TLSKEY, string(serverKeyPEM))
		os.Setenv(TLSCERT, string(serverCertPEM))
		config, err := CreateTlsConfig()
		assert.NotNil(t, err)
		assert.Nil(t, config)
		os.Unsetenv(ROOTCA)
		os.Unsetenv(MTLSCERT)
		os.Unsetenv(MTLSKEY)
		os.Unsetenv(TLSKEY)
		os.Unsetenv(TLSCERT)
	})

	t.Run("CannotLoadTLS", func(t *testing.T) {
		os.Setenv(ROOTCA, string(caPEM))
		os.Setenv(MTLSCERT, string(serverCertPEM))
		os.Setenv(MTLSKEY, string(serverKeyPEM))
		os.Setenv(TLSKEY, "")
		os.Setenv(TLSCERT, "")
		config, err := CreateTlsConfig()
		assert.NotNil(t, err)
		assert.Nil(t, config)
		os.Unsetenv(ROOTCA)
		os.Unsetenv(MTLSCERT)
		os.Unsetenv(MTLSKEY)
		os.Unsetenv(TLSKEY)
		os.Unsetenv(TLSCERT)
	})
}
