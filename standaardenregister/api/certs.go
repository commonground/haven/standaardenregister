package api

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"net/http"
	"os"
)

const (
	ROOTCA          = "ROOTCA"
	TLSKEY          = "TLS_KEY"
	TLSCERT         = "TLS_CERT"
	MTLSKEY         = "MTLS_KEY"
	MTLSCERT        = "MTLS_CERT"
	DELEGATEDHEADER = "X-Delegated-Cert"
)

func extractSecondaryCertificate(r *http.Request) (*CertModel, error) {
	encodedCert := r.Header.Get(DELEGATEDHEADER)
	if encodedCert == "" {
		return nil, nil
	}

	decodedBytes, err := base64.StdEncoding.DecodeString(encodedCert)
	if err != nil {
		return nil, fmt.Errorf("failed to decode base64 certificate: %v", err)
	}

	block, _ := pem.Decode(decodedBytes)
	if block == nil {
		return nil, fmt.Errorf("failed to parse certificate PEM")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse certificate: %v", err)
	}

	subject := cert.Subject

	if subject.SerialNumber == "" {
		return nil, fmt.Errorf("client certificate missing serial number")
	}
	if len(subject.Organization) == 0 || subject.Organization[0] == "" {
		return nil, fmt.Errorf("client certificate missing organization")
	}

	return &CertModel{
		OIN:          subject.SerialNumber,
		Organisation: subject.Organization[0],
	}, nil
}

func extractClientCertificate(tlsInfo *tls.ConnectionState) (*CertModel, error) {
	if tlsInfo == nil || len(tlsInfo.PeerCertificates) == 0 {
		return nil, fmt.Errorf("no client certificate provided")
	}

	clientCert := tlsInfo.PeerCertificates[0]
	subject := clientCert.Subject

	if subject.SerialNumber == "" {
		return nil, fmt.Errorf("client certificate missing serial number")
	}
	if len(subject.Organization) == 0 || subject.Organization[0] == "" {
		return nil, fmt.Errorf("client certificate missing organization")
	}

	return &CertModel{
		OIN:          subject.SerialNumber,
		Organisation: subject.Organization[0],
	}, nil
}

func LoadX509KeyPairFromEnv(certEnv, keyEnv string) (tls.Certificate, error) {
	cert := os.Getenv(certEnv)
	key := os.Getenv(keyEnv)
	if cert == "" || key == "" {
		return tls.Certificate{}, fmt.Errorf("environment variables %s and %s must be set", certEnv, keyEnv)
	}
	return tls.X509KeyPair([]byte(cert), []byte(key))
}

func CreateTlsConfig() (*tls.Config, error) {
	caCertFile := os.Getenv(ROOTCA)
	if caCertFile == "" {
		return nil, fmt.Errorf("environment variable ROOTCA must be set")
	}

	// Load the TLS certs to be used for non-mTLS traffic (Let's Encrypt)
	tlsCerts, err := LoadX509KeyPairFromEnv(TLSCERT, TLSKEY)
	if err != nil {
		return nil, err
	}
	// Load the server certificate and private key for optional mTLS
	mtlsCert, err := LoadX509KeyPairFromEnv(MTLSCERT, MTLSKEY)
	if err != nil {
		return nil, err
	}

	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM([]byte(caCertFile))

	// Create a TLS configuration with the Let's Encrypt certificate first
	return &tls.Config{
		Certificates: []tls.Certificate{tlsCerts, mtlsCert}, // Regular TLS cert first
		ClientAuth:   tls.VerifyClientCertIfGiven,
		ClientCAs:    certPool,
		MinVersion:   tls.VersionTLS12,
	}, nil
}
