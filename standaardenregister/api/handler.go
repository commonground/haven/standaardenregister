package api

import (
	"crypto/rsa"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"gitlab.com/haven/standaardenregister/pkg/repo"
	"net/http"
	"strings"
	"time"
)

func ListChecks(c *gin.Context) {
	db, exists := c.Get("db")
	if !exists {
		c.JSON(http.StatusBadGateway, ApiError{
			ErrorCode:    502,
			ErrorMessage: "Database connection not found",
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	authHeader := c.GetHeader("Authorization")
	if authHeader == "" {
		c.JSON(http.StatusUnauthorized, ApiError{
			ErrorCode:    401,
			ErrorMessage: "no Authorization in request",
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	publicKey, exists := c.Get("publicKey")
	if !exists {
		c.JSON(http.StatusInternalServerError, ApiError{
			ErrorCode:    500,
			ErrorMessage: "publicKey could not be found in context",
			Method:       "POST",
			Path:         "ListChecks",
		})
		return
	}

	// The Authorization header should be in the format `Bearer <token>`
	tokenString := strings.TrimPrefix(authHeader, "Bearer ")

	token, err := extractToken(tokenString, publicKey.(*rsa.PublicKey))
	if err != nil {
		c.JSON(http.StatusUnauthorized, ApiError{
			ErrorCode:    401,
			ErrorMessage: err.Error(),
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		c.JSON(http.StatusUnauthorized, ApiError{
			ErrorCode:    401,
			ErrorMessage: "invalid token",
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	oin, ok := claims["oin"].(string)
	if !ok {
		c.JSON(http.StatusUnauthorized, ApiError{
			ErrorCode:    401,
			ErrorMessage: "OIN claim missing in token",
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	admin, ok := claims["admin"].(bool)
	if !ok {
		c.JSON(http.StatusUnauthorized, ApiError{
			ErrorCode:    401,
			ErrorMessage: "admin claim missing in token",
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	var entries []repo.CheckCompliance
	var retrieveError error
	if admin {
		// Find all entries as an admin
		entries, retrieveError = db.(repo.Database).RetrieveChecks()
	} else {
		// Find all entries for the OIN
		entries, retrieveError = db.(repo.Database).RetrieveAllChecksFromOrganisation(oin)
	}

	if retrieveError != nil {
		c.JSON(http.StatusBadRequest, ApiError{
			ErrorCode:    400,
			ErrorMessage: retrieveError.Error(),
			Method:       "GET",
			Path:         "ListChecks",
		})
		return
	}

	response := parseToDTOSlice(entries)

	if c.GetHeader("HX-Request") != "" {
		c.HTML(http.StatusOK, "table.gohtml", gin.H{"Checks": response})
	} else {
		c.JSON(http.StatusOK, response)
	}
}

func CreateCheck(c *gin.Context) {
	var requestBody RequestModel
	if err := c.ShouldBindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, ApiError{
			ErrorCode:    400,
			ErrorMessage: err.Error(),
			Method:       "POST",
			Path:         "CreateCheck",
		})
		return
	}

	db, exists := c.Get("db")
	if !exists {
		c.JSON(http.StatusBadGateway, ApiError{
			ErrorCode:    502,
			ErrorMessage: "Database connection not found",
			Method:       "POST",
			Path:         "CreateCheck",
		})
		return
	}

	standard, err := db.(repo.Database).GetStandardByID(uint(requestBody.Standard))
	if err != nil {
		c.JSON(http.StatusBadRequest, ApiError{
			ErrorCode:    400,
			ErrorMessage: fmt.Sprintf("could not get standard with id: %v error: %s", requestBody.Standard, err.Error()),
			Method:       "POST",
			Path:         "CreateCheck",
		})
		return
	}
	newUuid := uuid.New().String()

	newTelemetry := repo.CheckCompliance{
		HavenHostingId: newUuid,
		HavenHosting: repo.HavenHosting{
			Id:                newUuid,
			ClusterIdentifier: requestBody.ClusterIdentifier,
			Command:           requestBody.Command,
			Platform:          requestBody.Platform,
			Compliant:         requestBody.Compliant,
		},
		StandardID:      uint(requestBody.Standard),
		StandardVersion: requestBody.StandardVersion,
		Meta:            requestBody.Meta,
	}

	if c.Request.TLS != nil && len(c.Request.TLS.PeerCertificates) > 0 {
		cert, err := extractClientCertificate(c.Request.TLS)
		if err != nil {
			c.JSON(http.StatusBadRequest, ApiError{
				ErrorCode:    400,
				ErrorMessage: err.Error(),
				Method:       "POST",
				Path:         "CreateCheck",
			})
			return
		}

		newTelemetry.OIN = cert.OIN
		newTelemetry.Organisation = cert.Organisation

		delegated, err := extractSecondaryCertificate(c.Request)
		if delegated != nil {
			newTelemetry.DelegatedOrganisation = delegated.Organisation
			newTelemetry.DelegatedOIN = delegated.OIN
		}
	}

	check, err := db.(repo.Database).CreateCertifiedCheck(newTelemetry)
	if err != nil {
		c.JSON(http.StatusBadRequest, ApiError{
			ErrorCode:    400,
			ErrorMessage: err.Error(),
			Method:       "POST",
			Path:         "CreateCheck",
		})
		return
	}

	response := parseToDTO(check)
	response.Standard.Name = standard.Name

	c.JSON(http.StatusCreated, response)
}

func CreateToken(c *gin.Context) {
	cert, err := extractClientCertificate(c.Request.TLS)
	if err != nil {
		c.JSON(http.StatusBadRequest, ApiError{
			ErrorCode:    400,
			ErrorMessage: err.Error(),
			Method:       "POST",
			Path:         "CreateToken",
		})
		return
	}

	signingKey, exists := c.Get("privateKey")
	if !exists {
		c.JSON(http.StatusInternalServerError, ApiError{
			ErrorCode:    500,
			ErrorMessage: "privateKey could not be found in context",
			Method:       "POST",
			Path:         "CreateToken",
		})
		return
	}

	tokenExpiration, exists := c.Get("tokenExpiration")
	if !exists {
		c.JSON(http.StatusInternalServerError, ApiError{
			ErrorCode:    500,
			ErrorMessage: "tokenTimer could not be found in context",
			Method:       "POST",
			Path:         "CreateToken",
		})
		return
	}

	fromCtx := tokenExpiration.(string)
	expires, err := time.ParseDuration(fromCtx)
	if err != nil {
		c.JSON(http.StatusInternalServerError, ApiError{
			ErrorCode:    500,
			ErrorMessage: err.Error(),
			Method:       "POST",
			Path:         "CreateToken",
		})
		return
	}

	var isAdmin bool

	username, password, hasAuth := c.Request.BasicAuth()
	if hasAuth {
		adminCredentials, exists := c.Get("adminCredentials")
		if !exists {
			c.JSON(http.StatusInternalServerError, ApiError{
				ErrorCode:    500,
				ErrorMessage: "adminCredentials not found in context",
				Method:       "POST",
				Path:         "CreateToken",
			})
			return
		}

		adminCreds, ok := adminCredentials.(map[string]string)
		if !ok {
			c.JSON(http.StatusInternalServerError, ApiError{
				ErrorCode:    500,
				ErrorMessage: "adminCredentials in context but not of type map[string]string",
				Method:       "POST",
				Path:         "CreateToken",
			})
			return
		}

		expectedPassword, userExists := adminCreds[username]
		if userExists && password == expectedPassword {
			isAdmin = true
		}
	}

	jwt, err := GenerateJWT(cert.OIN, cert.Organisation, isAdmin, signingKey.(*rsa.PrivateKey), expires)
	if err != nil {
		c.JSON(http.StatusInternalServerError, ApiError{
			ErrorCode:    500,
			ErrorMessage: err.Error(),
			Method:       "POST",
			Path:         "CreateToken",
		})
		return
	}
	response := JWTResponse{
		Token:     jwt,
		TokenType: "Bearer",
		ExpiresIn: int(expires.Seconds()),
	}

	c.JSON(http.StatusCreated, response)
}

func Healthz(c *gin.Context) {
	db, exists := c.Get("db")
	if !exists {
		c.JSON(http.StatusBadGateway, ApiError{
			ErrorCode:    502,
			ErrorMessage: "Database connection not found",
			Method:       "GET",
			Path:         "Status",
		})

		return
	}

	dbStatus := db.(repo.Database).HealthCheck()

	status := HealthzReport{
		Healthy:      dbStatus.Healthy,
		TimeStamp:    time.Now().UTC(),
		DbConnection: dbStatus,
	}

	c.JSON(http.StatusOK, status)
}

func Validate(c *gin.Context) {
	_, err := extractClientCertificate(c.Request.TLS)
	if err != nil {
		c.JSON(http.StatusBadRequest, Validation{
			CertsValid: false,
		})
		return
	}

	c.JSON(http.StatusOK, Validation{CertsValid: true})
}
