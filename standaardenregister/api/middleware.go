package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// checkProbeAccess is a middleware to block traffic to the healthz endpoint
// it still allows access for kube probes
func checkProbeAccess() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.URL.Path == "/healthz" {
			userAgent := c.GetHeader("User-Agent")
			if !strings.HasPrefix(userAgent, "kube-probe/") {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
					"message": "reserved endpoint",
				})
				return
			}
		}
		c.Next()
	}
}
