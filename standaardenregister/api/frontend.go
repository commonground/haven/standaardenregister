package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Homepage(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "Standaardenregister",
	})
}
