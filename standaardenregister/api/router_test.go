package api

import (
	"bytes"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	uuid2 "github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/haven/standaardenregister/pkg/repo"
	"go.uber.org/mock/gomock"
	"gorm.io/gorm"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

func setupTLSTestServer(router *gin.Engine, serverCert tls.Certificate, caCert *x509.Certificate) *httptest.Server {
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{serverCert},
		ClientAuth:   tls.VerifyClientCertIfGiven,
		ClientCAs:    x509.NewCertPool(),
	}

	tlsConfig.ClientCAs.AddCert(caCert)

	server := httptest.NewUnstartedServer(router)
	server.TLS = tlsConfig
	server.StartTLS()

	return server
}

func setupRouter(mockDb *repo.MockDatabase, user, password string) (*gin.Engine, *httptest.ResponseRecorder) {
	if gin.Mode() != gin.ReleaseMode {
		gin.SetMode(gin.TestMode)
	}

	caCert, caPrivKey, err := generateCACertificate()
	if err != nil {
		log.Fatalf("Failed to generate CA certificate: %v", err)
	}

	testCert, err := generateTestCertificateFromCa(caCert, caPrivKey, false, "TestOrg", "TestSerial")
	if err != nil {
		log.Fatalf("Failed to generate test certificate: %v", err)
	}

	_, certPEM, keyPEM := generatePEMFiles(caCert, testCert)

	os.Setenv(MTLSCERT, string(certPEM))
	os.Setenv(MTLSKEY, string(keyPEM))

	router := CreateRouter(mockDb, user, password, "2h")
	recorder := httptest.NewRecorder()

	return router, recorder
}

func TestOpenAPIRoute(t *testing.T) {
	router, recorder := setupRouter(nil, "user", "password")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/openapi.yaml", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, recorder.Code)
}

func TestRedocHTMLRoute(t *testing.T) {
	router, recorder := setupRouter(nil, "user", "password")

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/schema", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, recorder.Code)
}

func TestHealthzEndpoint(t *testing.T) {
	t.Run("DbUnhealthy", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		// Set up expected behavior on the mock
		mockDB.EXPECT().HealthCheck().Return(repo.DbConnectionStatus{
			Version: "12.0",
			Healthy: false,
		})

		router, recorder := setupRouter(mockDB, "user", "password")

		req, _ := http.NewRequest(http.MethodGet, "/healthz", nil)

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusOK, recorder.Code)

		var status HealthzReport
		err := json.Unmarshal(recorder.Body.Bytes(), &status)
		assert.Nil(t, err)

		assert.False(t, status.Healthy)
	})

	t.Run("DbHealthy", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		// Set up expected behavior on the mock
		mockDB.EXPECT().HealthCheck().Return(repo.DbConnectionStatus{
			Version: "12.0",
			Healthy: true,
		})

		router, recorder := setupRouter(mockDB, "user", "password")

		req, _ := http.NewRequest(http.MethodGet, "/healthz", nil)

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusOK, recorder.Code)

		var status HealthzReport
		err := json.Unmarshal(recorder.Body.Bytes(), &status)
		assert.Nil(t, err)

		assert.True(t, status.Healthy)
	})

	t.Run("MiddlewareActiveInReleaseMode", func(t *testing.T) {
		gin.SetMode(gin.ReleaseMode)
		router, recorder := setupRouter(nil, "user", "password")
		req, _ := http.NewRequest(http.MethodGet, "/healthz", nil)

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusForbidden, recorder.Code)

	})

	t.Run("DbNotFound", func(t *testing.T) {
		path := "/api/v1/healthz"

		gin.SetMode(gin.TestMode)
		router := gin.Default()
		router.GET(path, Healthz)
		recorder := httptest.NewRecorder()

		req, _ := http.NewRequest(http.MethodGet, path, nil)

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusBadGateway, recorder.Code)
	})
}

func TestCreateToken(t *testing.T) {
	user := "testuser"
	password := "testpassword"
	org := "testOrganisation"
	serial := "1234567890"

	caCert, caPrivKey, err := generateCACertificate()
	assert.Nil(t, err)

	clientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, org, serial)
	assert.Nil(t, err)

	serverCert, err := generateTestCertificateFromCa(caCert, caPrivKey, false, org, serial)
	assert.Nil(t, err)

	t.Run("HappyPathNonAdmin", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		router, _ := setupRouter(nil, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)

		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/auth/token", nil)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusCreated, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut JWTResponse
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)

		assert.NotNil(t, sut.Token)

		keys, err := LoadX509KeyPairFromEnv(MTLSCERT, MTLSKEY)
		assert.Nil(t, err)
		privateKey, ok := keys.PrivateKey.(*rsa.PrivateKey)
		assert.True(t, ok)

		publicKey := &privateKey.PublicKey

		parsed, err := extractToken(sut.Token, publicKey)
		assert.Nil(t, err)

		claims, ok := parsed.Claims.(jwt.MapClaims)
		assert.True(t, ok)
		admin, ok := claims["admin"].(bool)
		assert.True(t, ok)
		assert.False(t, admin)
	})

	t.Run("HappyPathAdmin", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		router, _ := setupRouter(nil, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)

		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/auth/token", nil)
		req.SetBasicAuth(user, password)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusCreated, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut JWTResponse
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)

		assert.NotNil(t, sut.Token)

		keys, err := LoadX509KeyPairFromEnv(MTLSCERT, MTLSKEY)
		assert.Nil(t, err)
		privateKey, ok := keys.PrivateKey.(*rsa.PrivateKey)
		assert.True(t, ok)

		publicKey := &privateKey.PublicKey

		parsed, err := extractToken(sut.Token, publicKey)
		assert.Nil(t, err)

		claims, ok := parsed.Claims.(jwt.MapClaims)
		assert.True(t, ok)
		admin, ok := claims["admin"].(bool)
		assert.True(t, ok)
		assert.True(t, admin)
	})
}

func TestListChecks(t *testing.T) {
	user := "testuser"
	password := "testpassword"
	org := "testOrganisation"
	serial := "1234567890"

	caCert, caPrivKey, err := generateCACertificate()
	assert.Nil(t, err)

	clientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, org, serial)
	assert.Nil(t, err)

	serverCert, err := generateTestCertificateFromCa(caCert, caPrivKey, false, org, serial)
	assert.Nil(t, err)

	t.Run("HappyPath", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		uuid := uuid2.New().String()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().RetrieveAllChecksFromOrganisation(serial).Return([]repo.CheckCompliance{
			{
				Model: gorm.Model{
					ID:        1,
					CreatedAt: time.Now().UTC(),
					UpdatedAt: time.Now().UTC(),
				},
				StandardID: 1,
				Standard: repo.Standard{
					Name: "test",
				},
				HavenHostingId: uuid,
				HavenHosting: repo.HavenHosting{
					Id:                uuid,
					ClusterIdentifier: "98d9371b-7ba8-43f2-bbb2-fc70e5ff8dfd",
					Command:           "test check",
					Platform:          "testplatform",
					Compliant:         false,
				},
				StandardVersion: "99.9.9",
				OIN:             "123",
				Organisation:    "An Org",
				Meta:            nil,
			},
		}, nil)

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)

		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		tokenReq, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/auth/token", nil)
		assert.Nil(t, err)
		tokenResp, err := client.Do(tokenReq)
		assert.Nil(t, err)
		jwtBody, err := io.ReadAll(tokenResp.Body)
		assert.Nil(t, err)

		var jwt JWTResponse
		err = json.Unmarshal(jwtBody, &jwt)
		assert.Nil(t, err)

		req, err := http.NewRequest(http.MethodGet, server.URL+"/api/v1/checks", nil)
		req.Header.Set("Authorization", "Bearer "+jwt.Token)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut []CheckCompliance
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)

		assert.Equal(t, uuid, sut[0].Uuid)
		assert.False(t, sut[0].Hosting.Compliant)
	})

	t.Run("DbNotFound", func(t *testing.T) {
		path := "/api/v1/checks"

		gin.SetMode(gin.TestMode)
		router := gin.Default()
		router.GET(path, ListChecks)
		recorder := httptest.NewRecorder()

		req, _ := http.NewRequest(http.MethodGet, path, nil)

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusBadGateway, recorder.Code)
	})

	t.Run("EndpointRequiresJWT", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)

		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodGet, server.URL+"/api/v1/checks", nil)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
	})

	t.Run("DbReturnsAnError", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().RetrieveAllChecksFromOrganisation(serial).Return(nil, fmt.Errorf("some error"))

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		tokenReq, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/auth/token", nil)
		assert.Nil(t, err)
		tokenResp, err := client.Do(tokenReq)
		assert.Nil(t, err)
		jwtBody, err := io.ReadAll(tokenResp.Body)
		assert.Nil(t, err)

		var jwt JWTResponse
		err = json.Unmarshal(jwtBody, &jwt)
		assert.Nil(t, err)

		req, err := http.NewRequest(http.MethodGet, server.URL+"/api/v1/checks", nil)
		req.Header.Set("Authorization", "Bearer "+jwt.Token)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var apiError ApiError
		err = json.Unmarshal(body, &apiError)
		assert.Nil(t, err)

		assert.Equal(t, http.StatusBadRequest, apiError.ErrorCode)
		assert.Equal(t, "some error", apiError.ErrorMessage)
	})
}

func TestCreateChecks(t *testing.T) {
	user := "testuser"
	password := "testpassword"
	org := "testOrganisation"
	serial := "1234567890"
	validRequestBody := RequestModel{
		Standard:        1,
		Compliant:       true,
		StandardVersion: "99.9.99",
		Meta: map[string]interface{}{
			"user": "pocuser",
		},
		ClusterIdentifier: "98d9371b-7ba8-43f2-bbb2-fc70e5ff8dfd",
		Command:           "testcli check",
		Platform:          "testplatform",
	}
	validRequestBodyAsBytes, err := json.Marshal(&validRequestBody)
	assert.Nil(t, err)

	caCert, caPrivKey, err := generateCACertificate()
	assert.Nil(t, err)

	clientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, org, serial)
	assert.Nil(t, err)

	serverCert, err := generateTestCertificateFromCa(caCert, caPrivKey, false, org, serial)
	assert.Nil(t, err)

	t.Run("HappyPath", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().GetStandardByID(uint(validRequestBody.Standard)).Return(repo.Standard{Name: "testHaven"}, nil)
		mockDB.EXPECT().CreateCertifiedCheck(gomock.AssignableToTypeOf(repo.CheckCompliance{})).
			Do(func(check repo.CheckCompliance) {
			}).
			Return(repo.CheckCompliance{
				HavenHosting: repo.HavenHosting{
					ClusterIdentifier: validRequestBody.ClusterIdentifier,
					Command:           validRequestBody.Command,
					Platform:          validRequestBody.Platform,
					Compliant:         validRequestBody.Compliant,
				},
				StandardVersion:       validRequestBody.StandardVersion,
				OIN:                   serial,
				Organisation:          org,
				DelegatedOIN:          "",
				DelegatedOrganisation: "",
				Meta:                  validRequestBody.Meta,
			}, nil)

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/checks", bytes.NewBuffer(validRequestBodyAsBytes))
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusCreated, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut CheckCompliance
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)

		assert.Equal(t, sut.OIN, serial)
		assert.Equal(t, sut.Organisation, org)
		assert.Equal(t, validRequestBody.Command, sut.Hosting.Command)
		assert.Equal(t, validRequestBody.Compliant, sut.Hosting.Compliant)
	})

	t.Run("HappyPathwithDelegatedCert", func(t *testing.T) {
		delegatedOrg := "delegatedOrg"
		delegatedOIN := "987654321"
		delegatedCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, delegatedOrg, delegatedOIN)
		assert.Nil(t, err)

		dCert := pem.EncodeToMemory(&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: delegatedCert.Certificate[0],
		})
		encodedCert := base64.StdEncoding.EncodeToString(dCert)

		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().GetStandardByID(uint(validRequestBody.Standard)).Return(repo.Standard{Name: "testHaven"}, nil)
		mockDB.EXPECT().CreateCertifiedCheck(gomock.AssignableToTypeOf(repo.CheckCompliance{})).
			Do(func(check repo.CheckCompliance) {
			}).
			Return(repo.CheckCompliance{
				HavenHosting: repo.HavenHosting{
					ClusterIdentifier: validRequestBody.ClusterIdentifier,
					Command:           validRequestBody.Command,
					Platform:          validRequestBody.Platform,
					Compliant:         validRequestBody.Compliant,
				},
				StandardVersion:       validRequestBody.StandardVersion,
				OIN:                   serial,
				Organisation:          org,
				DelegatedOIN:          delegatedOIN,
				DelegatedOrganisation: delegatedOrg,
				Meta:                  validRequestBody.Meta,
			}, nil)

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/checks", bytes.NewBuffer(validRequestBodyAsBytes))
		req.Header.Set(DELEGATEDHEADER, encodedCert)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusCreated, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut CheckCompliance
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)

		assert.Equal(t, sut.OIN, serial)
		assert.Equal(t, sut.Organisation, org)
		assert.Equal(t, validRequestBody.Command, sut.Hosting.Command)
		assert.Equal(t, validRequestBody.Compliant, sut.Hosting.Compliant)
		assert.Equal(t, delegatedOIN, sut.DelegatedOIN)
		assert.Equal(t, delegatedOrg, sut.DelegatedOrganisation)
	})

	t.Run("NoCertificateInRequestStillCreates", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().GetStandardByID(uint(validRequestBody.Standard)).Return(repo.Standard{Name: "testHaven"}, nil)
		mockDB.EXPECT().CreateCertifiedCheck(gomock.AssignableToTypeOf(repo.CheckCompliance{})).
			Do(func(check repo.CheckCompliance) {
			}).
			Return(repo.CheckCompliance{
				HavenHosting: repo.HavenHosting{
					ClusterIdentifier: validRequestBody.ClusterIdentifier,
					Command:           validRequestBody.Command,
					Platform:          validRequestBody.Platform,
					Compliant:         validRequestBody.Compliant,
				},
				StandardVersion:       validRequestBody.StandardVersion,
				OIN:                   "",
				Organisation:          "",
				DelegatedOIN:          "",
				DelegatedOrganisation: "",
				Meta:                  validRequestBody.Meta,
			}, nil)

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       nil,
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/checks", bytes.NewBuffer(validRequestBodyAsBytes))
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusCreated, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut CheckCompliance
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)

		assert.Equal(t, sut.OIN, "")
		assert.Equal(t, sut.Organisation, "")
		assert.Equal(t, validRequestBody.Command, sut.Hosting.Command)
		assert.Equal(t, validRequestBody.Compliant, sut.Hosting.Compliant)
	})

	t.Run("MissingOrgReturnsErr", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().GetStandardByID(uint(validRequestBody.Standard)).Return(repo.Standard{Name: "testHaven"}, nil)

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		newClientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, "", serial)
		assert.Nil(t, err)

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*newClientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/checks", bytes.NewBuffer(validRequestBodyAsBytes))
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var apiError ApiError
		err = json.Unmarshal(body, &apiError)
		assert.Nil(t, err)

		assert.Equal(t, http.StatusBadRequest, apiError.ErrorCode)
		assert.Contains(t, apiError.ErrorMessage, "organization")
	})

	t.Run("EmptyRequest", func(t *testing.T) {
		path := "/api/v1/checks"

		gin.SetMode(gin.TestMode)
		router := gin.Default()
		router.POST(path, CreateCheck)
		recorder := httptest.NewRecorder()

		req, _ := http.NewRequest(http.MethodPost, path, nil)

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusBadRequest, recorder.Code)
	})

	t.Run("DbNotFound", func(t *testing.T) {
		path := "/api/v1/checks"

		gin.SetMode(gin.TestMode)
		router := gin.Default()
		router.POST(path, CreateCheck)
		recorder := httptest.NewRecorder()

		req, _ := http.NewRequest(http.MethodPost, path, bytes.NewBuffer(validRequestBodyAsBytes))

		router.ServeHTTP(recorder, req)

		// Check the response status code
		assert.Equal(t, http.StatusBadGateway, recorder.Code)
	})

	t.Run("DbReturnsAnError", func(t *testing.T) {
		mockCtrl := gomock.NewController(t)
		defer mockCtrl.Finish()

		mockDB := repo.NewMockDatabase(mockCtrl)
		mockDB.EXPECT().GetStandardByID(uint(validRequestBody.Standard)).Return(repo.Standard{Name: "testHaven"}, nil)
		mockDB.EXPECT().CreateCertifiedCheck(gomock.AssignableToTypeOf(repo.CheckCompliance{})).
			Do(func(check repo.CheckCompliance) {
			}).
			Return(repo.CheckCompliance{}, fmt.Errorf("some error"))

		router, _ := setupRouter(mockDB, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/checks", bytes.NewBuffer(validRequestBodyAsBytes))
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var apiError ApiError
		err = json.Unmarshal(body, &apiError)
		assert.Nil(t, err)

		assert.Equal(t, http.StatusBadRequest, apiError.ErrorCode)
		assert.Equal(t, "some error", apiError.ErrorMessage)
	})
}

func TestValidate(t *testing.T) {
	user := "testuser"
	password := "testpassword"
	org := "testOrganisation"
	serial := "1234567890"

	caCert, caPrivKey, err := generateCACertificate()
	assert.Nil(t, err)

	clientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, org, serial)
	assert.Nil(t, err)

	serverCert, err := generateTestCertificateFromCa(caCert, caPrivKey, false, org, serial)
	assert.Nil(t, err)

	t.Run("HappyPath", func(t *testing.T) {
		router, _ := setupRouter(nil, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*clientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/validate", nil)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut Validation
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)
		assert.True(t, sut.CertsValid)
	})

	t.Run("MissingSerialReturnsErr", func(t *testing.T) {
		router, _ := setupRouter(nil, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		newClientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, org, "")
		assert.Nil(t, err)

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*newClientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/validate", nil)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut Validation
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)
		assert.False(t, sut.CertsValid)
	})

	t.Run("MissingOrgReturnsErr", func(t *testing.T) {
		router, _ := setupRouter(nil, user, password)
		server := setupTLSTestServer(router, *serverCert, caCert)
		defer server.Close()

		newClientCert, err := generateTestCertificateFromCa(caCert, caPrivKey, true, "", serial)
		assert.Nil(t, err)

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{*newClientCert},
					InsecureSkipVerify: true, // Skip CA verification for test
				},
			},
		}

		req, err := http.NewRequest(http.MethodPost, server.URL+"/api/v1/validate", nil)
		assert.Nil(t, err)
		resp, err := client.Do(req)
		assert.Nil(t, err)

		// Check the response status code
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)

		// Read the response body
		body, err := io.ReadAll(resp.Body)
		assert.Nil(t, err)

		var sut Validation
		err = json.Unmarshal(body, &sut)
		assert.Nil(t, err)
		assert.False(t, sut.CertsValid)
	})
}
