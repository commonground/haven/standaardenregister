package api

import (
	"gitlab.com/haven/standaardenregister/pkg/repo"
	"time"
)

type HealthzReport struct {
	Healthy      bool                    `json:"healthy"`
	TimeStamp    time.Time               `json:"timestamp"`
	DbConnection repo.DbConnectionStatus `json:"database"`
}

type StandardModel struct {
	Name string `json:"name"`
}

type CertModel struct {
	OIN          string `json:"oin"`
	Organisation string `json:"organisation"`
}

// RequestModel represents the structure of the POST request payload
type RequestModel struct {
	// Fields for CertifiedTelemetry
	Standard        int                    `json:"standard"`
	Compliant       bool                   `json:"compliant"`
	StandardVersion string                 `json:"standardVersion"`
	Meta            map[string]interface{} `json:"meta"`

	// Fields to get more info about the usage
	ClusterIdentifier string `json:"clusterIdentifier"`
	Command           string `json:"command"`
	Platform          string `json:"platform"`
}

type ApiError struct {
	ErrorCode    int    `json:"code"`
	ErrorMessage string `json:"message"`
	Method       string `json:"method"`
	Path         string `json:"path"`
}

type Validation struct {
	CertsValid bool `json:"certsValid"`
}

type JWTResponse struct {
	Token     string `json:"token"`
	TokenType string `json:"tokenType"`
	ExpiresIn int    `json:"expiresIn"`
}

type CheckCompliance struct {
	CreatedAt             time.Time              `json:"createdAt"`
	UpdatedAt             time.Time              `json:"updatedAt"`
	Standard              Standard               `json:"standard"`
	Hosting               Hosting                `json:"hosting"`
	OIN                   string                 `json:"oin,omitempty"`
	Organisation          string                 `json:"organisation,omitempty"`
	DelegatedOIN          string                 `json:"delegatedOIN,omitempty"`
	DelegatedOrganisation string                 `json:"delegatedOrganisation,omitempty"`
	Uuid                  string                 `json:"uuid"`
	Meta                  map[string]interface{} `json:"meta,omitempty"`
}

type Standard struct {
	Name string `json:"name"`
}

type Hosting struct {
	ClusterIdentifier string `json:"clusterIdentifier"`
	Command           string `json:"command"`
	Platform          string `json:"platform"`
	Compliant         bool   `json:"compliant"`
}

func parseToDTO(input repo.CheckCompliance) CheckCompliance {
	check := CheckCompliance{
		CreatedAt: input.CreatedAt,
		UpdatedAt: input.UpdatedAt,
		Uuid:      input.HavenHostingId,
		Standard: Standard{
			Name: input.Standard.Name,
		},
		Hosting: Hosting{
			ClusterIdentifier: input.HavenHosting.ClusterIdentifier,
			Command:           input.HavenHosting.Command,
			Platform:          input.HavenHosting.Platform,
			Compliant:         input.HavenHosting.Compliant,
		},
	}

	if input.OIN != "" {
		check.OIN = input.OIN
	}

	if input.DelegatedOIN != "" {
		check.DelegatedOIN = input.DelegatedOIN
	}

	if input.DelegatedOrganisation != "" {
		check.DelegatedOrganisation = input.DelegatedOrganisation
	}

	if input.Organisation != "" {
		check.Organisation = input.Organisation
	}

	if input.Meta != nil {
		check.Meta = input.Meta
	}

	return check
}
func parseToDTOSlice(input []repo.CheckCompliance) []CheckCompliance {
	var checks []CheckCompliance

	for _, in := range input {
		check := parseToDTO(in)
		checks = append(checks, check)
	}

	return checks
}
