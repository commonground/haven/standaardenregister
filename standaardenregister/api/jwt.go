package api

import (
	"crypto/rsa"
	"fmt"
	"github.com/golang-jwt/jwt"
	"time"
)

type StandaardenRegisterClaims struct {
	OIN          string `json:"oin"`
	Organisation string `json:"organisation"`
	Admin        bool   `json:"admin"`
	jwt.StandardClaims
}

func GenerateJWT(oin, organisation string, admin bool, signingKey *rsa.PrivateKey, validity time.Duration) (string, error) {
	claims := StandaardenRegisterClaims{
		oin,
		organisation,
		admin,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(validity).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	signedToken, err := token.SignedString(signingKey)
	if err != nil {
		return "", err
	}
	return signedToken, nil
}

// extractToken parses and verifies a JWT using the specified signing key. The function ensures that the token
// uses the expected RSA signing method. If the token's signature is valid, the function returns the parsed token.
// If the parsing or signature verification fails, an error is returned. The signingKey is derived from a secure source related to the mTLS flow.
func extractToken(tokenString string, publicKey *rsa.PublicKey) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return publicKey, nil
	})

	if err != nil {
		return nil, err
	}

	return token, nil
}
