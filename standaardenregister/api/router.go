package api

import (
	"crypto/rsa"
	"embed"
	"github.com/gin-gonic/gin"
	"gitlab.com/haven/standaardenregister/pkg/repo"
	"html/template"
	"log"
	"net/http"
)

//go:embed docs
var docs embed.FS

//go:embed templates/*
var templatesFS embed.FS

func CreateRouter(db repo.Database, user, password, tokenExpiration string) *gin.Engine {
	router := gin.New()
	router.Use(
		gin.LoggerWithWriter(gin.DefaultWriter, "/healthz"),
		gin.Recovery(),
	)
	// for local development we don't need to block access
	if gin.Mode() == gin.ReleaseMode {
		router.Use(checkProbeAccess())
	}

	keys, _ := LoadX509KeyPairFromEnv(MTLSCERT, MTLSKEY)
	privateKey, ok := keys.PrivateKey.(*rsa.PrivateKey)
	if !ok {
		log.Fatalf("Private key is not of type *rsa.PrivateKey")
	}

	publicKey := &privateKey.PublicKey

	// Database injected into Gin context
	router.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Set("privateKey", privateKey)
		c.Set("publicKey", publicKey)
		c.Set("tokenExpiration", tokenExpiration)
		c.Set("adminCredentials", map[string]string{user: password})
		c.Next()
	})

	tmpl := template.Must(template.New("").ParseFS(templatesFS, "templates/*"))
	router.SetHTMLTemplate(tmpl)

	// Public routes
	router.GET("/healthz", Healthz)
	router.GET("/api/v1/checks", ListChecks)
	router.POST("/api/v1/checks", CreateCheck)
	router.POST("/api/v1/telemetry", CreateCheck)
	router.POST("/api/v1/validate", Validate)

	// Authentication routes
	router.POST("/api/v1/auth/token", CreateToken)

	// Add a route for the homepage
	router.GET("/", Homepage)

	// Openapi routes
	openAPISpec, _ := docs.ReadFile("docs/openapi.yaml")
	router.GET("/api/v1/openapi.yaml", func(c *gin.Context) {
		c.Data(http.StatusOK, "application/yaml", openAPISpec)
	})

	// Serving the embedded Redoc HTML
	redocHTML, _ := docs.ReadFile("docs/redoc.html")
	router.GET("/api/v1/schema", func(c *gin.Context) {
		c.Data(http.StatusOK, "text/html", redocHTML)
	})

	return router
}
