package repo

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"os"
)

// go install go.uber.org/mock/mockgen@latest
//go:generate mockgen -destination=mock.go -package=repo -source=impl.go

type Database interface {
	AutoMigrate() error
	RetrieveChecks() ([]CheckCompliance, error)
	GetStandardByID(id uint) (Standard, error)
	CreateCertifiedCheck(compliance CheckCompliance) (CheckCompliance, error)
	RetrieveAllChecksFromOrganisation(oin string) ([]CheckCompliance, error)
	EnsureDevStandardExists() error
	HealthCheck() DbConnectionStatus
}

func CreateAndMigrate() (Database, error) {
	user := getEnv("DB_USER", "haven")
	password := getEnv("DB_PASSWORD", "isitsecret?isitsafe?")
	dbname := getEnv("DB_NAME", "standaardenregister")
	host := getEnv("DB_HOST", "localhost")
	port := getEnv("DB_PORT", "5432")
	sslmode := getEnv("DB_SSLMODE", "require")

	dsn := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=%s", user, password, dbname, host, port, sslmode)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	gormDB := &GormDatabase{DB: db}
	err = gormDB.AutoMigrate()
	if err != nil {
		log.Print(err)
		return nil, err
	}

	if err := gormDB.EnsureDevStandardExists(); err != nil {
		return nil, err
	}

	return gormDB, nil
}

func getEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}
