package repo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"gorm.io/gorm"
	"reflect"
)

const (
	HAVENHOSTING    string = "haven_hosting"
	CHECKCOMPLIANCE string = "check"
	STANDARD        string = "standard"
)

type JSONMap map[string]interface{}

func (jm *JSONMap) Scan(src interface{}) error {
	if bytes, ok := src.([]byte); ok {
		return json.Unmarshal(bytes, jm)
	}
	return errors.New("failed to unmarshal JSON from DB")
}

func (jm JSONMap) Value() (driver.Value, error) {
	if jm == nil {
		return nil, nil
	}
	return json.Marshal(jm)
}

type CheckCompliance struct {
	gorm.Model
	StandardID            uint         // Use StandardID as the foreign key to Standard
	Standard              Standard     `gorm:"foreignKey:StandardID"`
	HavenHostingId        string       `gorm:"type:uuid;not null;uniqueIndex"` // Ensure it's a string, suitable for UUIDs
	HavenHosting          HavenHosting `gorm:"foreignKey:HavenHostingId"`
	StandardVersion       string       `gorm:"size:255"`
	OIN                   string
	Organisation          string
	DelegatedOIN          string
	DelegatedOrganisation string
	Meta                  JSONMap `gorm:"type:jsonb"`
}

type Standard struct {
	gorm.Model
	Name string
}

type HavenHosting struct {
	Id                string `gorm:"type:uuid;primary_key;default:public.uuid_generate_v4()"`
	ClusterIdentifier string `gorm:"size:255"`
	Command           string `gorm:"size:255"`
	Platform          string `gorm:"size:255"`
	Compliant         bool
}

type DbConnectionStatus struct {
	Version string `json:"version"`
	Healthy bool   `json:"healthy"`
}

func (c *CheckCompliance) TableName() string {
	return CHECKCOMPLIANCE
}

func (s *Standard) TableName() string {
	return STANDARD
}

func (s *HavenHosting) TableName() string {
	return HAVENHOSTING
}

func GetStructName(i interface{}) string {
	return reflect.TypeOf(i).Elem().Name()
}
