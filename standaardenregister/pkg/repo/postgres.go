package repo

import (
	"gorm.io/gorm"
	"log"
)

type GormDatabase struct {
	DB *gorm.DB
}

func (gdb *GormDatabase) HealthCheck() DbConnectionStatus {
	var version string
	// Raw SQL query to get the PostgreSQL version
	gdb.DB.Raw("SELECT version();").Scan(&version)

	var dbStatus DbConnectionStatus
	dbStatus.Version = version

	sqlDB, err := gdb.DB.DB()
	if err != nil {
		dbStatus.Healthy = false
		return dbStatus
	}

	err = sqlDB.Ping()
	if err != nil {
		dbStatus.Healthy = false
	} else {
		dbStatus.Healthy = true
	}

	return dbStatus
}

func (gdb *GormDatabase) AutoMigrate() error {
	return gdb.DB.AutoMigrate(
		&CheckCompliance{},
		&Standard{},
		&HavenHosting{},
	)
}

func (gdb *GormDatabase) RetrieveChecks() ([]CheckCompliance, error) {
	var checks []CheckCompliance
	err := gdb.DB.Preload(GetStructName(&Standard{})).Preload(GetStructName(&HavenHosting{})).Find(&checks).Error
	return checks, err
}

func (gdb *GormDatabase) CreateCertifiedCheck(check CheckCompliance) (CheckCompliance, error) {
	t := gdb.DB.Create(&check)
	return check, t.Error
}

func (gdb *GormDatabase) CreateStandard(standardName string) error {
	standard := Standard{
		Model: gorm.Model{ID: 1},
		Name:  standardName,
	}

	return gdb.DB.Create(&standard).Error
}

func (gdb *GormDatabase) GetStandardByID(id uint) (Standard, error) {
	var standard Standard
	result := gdb.DB.First(&standard, id)

	if result.Error != nil {
		return Standard{}, result.Error
	}

	return standard, nil
}

func (gdb *GormDatabase) RetrieveAllChecksFromOrganisation(oin string) ([]CheckCompliance, error) {
	var checks []CheckCompliance
	err := gdb.DB.Preload(GetStructName(&Standard{})).Preload(GetStructName(&HavenHosting{})).Where("oin = ?", oin).Find(&checks).Error
	return checks, err
}

func (gdb *GormDatabase) EnsureDevStandardExists() error {
	// Check if the specific Standard already exists
	var count int64
	gdb.DB.Model(&Standard{}).Where("id = ?", 1).Count(&count)

	if count > 0 {
		log.Print("standard with id 1 already exists so no action taken")
		return nil
	}

	log.Print("creating standard standard with id 1")

	// Create the Standard as it does not exist
	standard := Standard{
		Model: gorm.Model{ID: 1},
		Name:  "haven",
	}

	return gdb.DB.Create(&standard).Error
}
