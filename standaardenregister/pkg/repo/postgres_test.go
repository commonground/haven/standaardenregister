package repo

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"testing"
)

func TestHealthCheck(t *testing.T) {
	// Create mock db connection
	expected := "PostgreSQL 15.9"
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)

	defer db.Close()

	mock.ExpectQuery("SELECT version()").WillReturnRows(sqlmock.NewRows([]string{"version"}).AddRow(expected))

	gdb, err := gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	assert.Nil(t, err)

	gormDb := &GormDatabase{DB: gdb}
	status := gormDb.HealthCheck()

	assert.Equal(t, expected, status.Version)
	assert.True(t, status.Healthy)
}

func TestCreateCertifiedCheck(t *testing.T) {
	// Create mock db connection
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)

	defer db.Close()

	standardVersion := "1.1.1.1.1"
	standardName := "test"
	oin := "thisisanoin"
	org := "myorg"

	// Define the CheckCompliance that we will use as an argument for the function
	check := CheckCompliance{
		StandardID: 1,
		Standard: Standard{
			Name: standardName,
		},
		DelegatedOrganisation: "",
		DelegatedOIN:          "",
		HavenHostingId:        "something",
		HavenHosting: HavenHosting{
			Id:                "something",
			ClusterIdentifier: "something",
			Command:           "something",
			Platform:          "something",
			Compliant:         false,
		},
		StandardVersion: standardVersion,
		OIN:             oin,
		Organisation:    org,
		Meta:            nil,
	}

	gdb, err := gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	assert.Nil(t, err)
	gormDb := &GormDatabase{DB: gdb}

	mock.ExpectBegin()
	mock.ExpectQuery(`^INSERT INTO \"standard\"`). // assuming this is the correct order
							WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), standardName).
							WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1)) // simulates 'RETURNING "id"' clause

	mock.ExpectQuery(`^INSERT INTO \"haven_hosting\"`).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg()).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1)) // simulates 'RETURNING "id"' clause

	mock.ExpectQuery(`^INSERT INTO \"check\"`).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), standardVersion, oin, org, "", "", nil).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1)) // simulates 'RETURNING "id"' clause

	mock.ExpectCommit()

	_, err = gormDb.CreateCertifiedCheck(check)

	assert.Nil(t, err)
	assert.Nil(t, mock.ExpectationsWereMet())
}

func TestCreateStandard(t *testing.T) {
	// Create mock db connection
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)

	defer db.Close()

	standardName := "testStandard"

	gdb, err := gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	assert.Nil(t, err)

	gormDb := &GormDatabase{DB: gdb}

	mock.ExpectBegin()

	mock.ExpectQuery(`^INSERT INTO \"standard\"`).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), standardName, sqlmock.AnyArg()).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1)) // simulates 'RETURNING "id"' clause

	mock.ExpectCommit()

	err = gormDb.CreateStandard(standardName)

	assert.Nil(t, err)
	assert.Nil(t, mock.ExpectationsWereMet())
}

func TestEnsureDevStandardExists(t *testing.T) {
	t.Run("StandardGetsCreatedAtStartup", func(t *testing.T) {
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		defer db.Close()

		gdb, err := gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
		require.NoError(t, err)
		gormDb := &GormDatabase{DB: gdb}

		mock.ExpectBegin()
		mock.ExpectQuery(`^INSERT INTO \"standard\"`).
			WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), "haven", sqlmock.AnyArg()).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
		mock.ExpectCommit()

		err = gormDb.EnsureDevStandardExists()

		assert.Nil(t, err)
		assert.Nil(t, mock.ExpectationsWereMet())
	})
	t.Run("IfStandardExistsNothingIsCreated", func(t *testing.T) {
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		defer db.Close()

		gdb, err := gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
		require.NoError(t, err)
		gormDb := &GormDatabase{DB: gdb}

		rows := sqlmock.NewRows([]string{"count"}).AddRow(1) // Simulate a count higher than 0
		mock.ExpectQuery("SELECT count\\(\\*\\) FROM \"standard\" WHERE id = \\$1 AND \"standard\".\"deleted_at\" IS NULL").WithArgs(1).WillReturnRows(rows)

		err = gormDb.EnsureDevStandardExists()

		assert.Nil(t, err)

		err = mock.ExpectationsWereMet()
		require.NoError(t, err)
	})
}
