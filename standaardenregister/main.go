package main

import (
	"fmt"
	"gitlab.com/haven/standaardenregister/api"
	"gitlab.com/haven/standaardenregister/pkg/repo"
	"log"
	"net/http"
	"os"
)

const (
	DEFAULTADMIN string = "admin"
)

func main() {
	adminName := os.Getenv("ADMIN_USER")
	if adminName == "" {
		adminName = DEFAULTADMIN
	}

	adminPassword := os.Getenv("ADMIN_PASSWORD")
	if adminPassword == "" {
		log.Fatal("cannot start server with an empty password please set ADMIN_PASSWORD")
	}

	tokenExpiration := os.Getenv("TOKEN_EXPIRATION")
	if tokenExpiration == "" {
		tokenExpiration = "2h"
	}

	tlsConfig, err := api.CreateTlsConfig()
	if err != nil {
		log.Fatalf("failed to create tls config: %v", err)
	}

	dbConnection, err := repo.CreateAndMigrate()
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	router := api.CreateRouter(dbConnection, adminName, adminPassword, tokenExpiration)
	// Create an HTTP server with mTLS enabled
	server := &http.Server{
		Addr:      ":5443",
		Handler:   router,
		TLSConfig: tlsConfig,
	}
	// Start the HTTPS server with mTLS
	fmt.Printf("Server is running on :5443")
	err = server.ListenAndServeTLS("", "")
	if err != nil {
		log.Fatal(err)
	}
}
