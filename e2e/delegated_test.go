package e2e

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/haven/standaardenregister/e2e/models"
	"net/http"
	"net/url"
)

func (s *StandaardenRegisterFixture) aCheckHasBeenCreatedAfterAHavenCliRunUsingMtlsAndDelegationForOrganisationAndOIN(organisation, OIN string) error {
	content, exists := s.fixtures["cli"]
	if !exists {
		return fmt.Errorf("fixture 'cli' not found")
	}

	u, err := url.JoinPath(s.standaardRegisterRoot, "api/v1/checks")
	if err != nil {
		return err
	}

	parsedURL, err := url.Parse(u)
	if err != nil {
		return err
	}

	mtls := s.createMTLSConfig()
	delegatedCert, err := GenerateTestCertificate(organisation, OIN)
	if err != nil {
		return err
	}
	dCert := pem.EncodeToMemory(&pem.Block{
		Type:  "CERTIFICATE",
		Bytes: delegatedCert.Certificate[0],
	})
	encodedCert := base64.StdEncoding.EncodeToString(dCert)

	response, err := PostRequestWithTLSAndDelegation(*parsedURL, content, "", encodedCert, mtls)
	if err != nil || response.StatusCode != http.StatusCreated {
		return err
	}

	var check models.Check
	err = json.NewDecoder(response.Body).Decode(&check)

	s.ctx = context.WithValue(s.ctx, CreatedUUID, check.Uuid)

	return nil
}

func (s *StandaardenRegisterFixture) theCheckHasTheDelegatedOrganisationAndOIN(organisation, OIN string) error {
	checks := s.ctx.Value(AdminResponse).([]models.Check)
	createdUUID := s.ctx.Value(CreatedUUID).(uuid.UUID).String()

	for _, check := range checks {
		if check.Uuid.String() == createdUUID {
			if *check.DelegatedOrganisation != organisation {
				return fmt.Errorf("organisation does not match. Expected: %s but was: %s", organisation, *check.Organisation)
			}

			if *check.DelegatedOIN != OIN {
				return fmt.Errorf("OIN does not match. Expected: %s but was: %s", OIN, *check.Oin)
			}

			break
		}

	}

	return nil
}
