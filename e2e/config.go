package e2e

import (
	"context"
	"embed"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

//go:embed fixtures/*.json
var fixtureFiles embed.FS

type StandaardenRegisterFixture struct {
	ctx                   context.Context
	fixtures              map[string][]byte
	standaardRegisterRoot string
	rootCA                []byte
	cert                  []byte
	key                   []byte
	env                   string
	username              string
	password              string
	unsafe                bool
}

const (
	STANDARDREGISTER     string = "https://localhost"
	KEYFROMENV                  = "MTLS_KEY"
	CERTFROMENV                 = "MTLS_CERT"
	ROOTCA                      = "ROOTCA"
	ENV                         = "ENV"
	ADMINPASSWORDFROMENV        = "ADMIN_PASSWORD"
	DEFAULTADMIN                = "admin"
)

func New() (*StandaardenRegisterFixture, error) {
	register := os.Getenv("STANDAARDREGISTER_SERVICE")
	if register == "" {
		register = STANDARDREGISTER
	}

	keyFromEnv := os.Getenv(KEYFROMENV)
	certFromEnv := os.Getenv(CERTFROMENV)
	rootCAFromEnv := os.Getenv(ROOTCA)
	env := os.Getenv(ENV)

	unsafe := false
	if env == "local" {
		unsafe = true
	}

	password := os.Getenv(ADMINPASSWORDFROMENV)

	fixture := &StandaardenRegisterFixture{
		ctx:                   context.Background(),
		fixtures:              make(map[string][]byte),
		standaardRegisterRoot: register,
		rootCA:                []byte(rootCAFromEnv),
		key:                   []byte(keyFromEnv),
		cert:                  []byte(certFromEnv),
		env:                   strings.ToLower(env),
		username:              DEFAULTADMIN,
		password:              password,
		unsafe:                unsafe,
	}

	err := fs.WalkDir(fixtureFiles, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			content, err := fs.ReadFile(fixtureFiles, path)
			if err != nil {
				return err
			}

			name := strings.TrimSuffix(filepath.Base(path), ".json")
			fixture.fixtures[name] = content
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return fixture, nil
}
