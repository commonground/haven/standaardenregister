Feature: Standard Flow
  In order to validate the workings of "standaardenregister"
  As a user of the haven cli
  The flow for users that do not use mtls should be verified

  Scenario: Creating an anonymous check
    Given an anonymous check has been created after a haven cli run
    When the check is searched using an admin token
    Then the created check can be found
