Feature: Delegated
  In order to validate the workings of "standaardenregister"
  As a user of the haven cli
  The flow for users using a delegated cert can be verified

  @wip
  Scenario Outline: Creating a check using MTLS with a delegated cert
    Given a check has been created after a haven cli run using mtls and delegation for organisation "<organisation>" and OIN  "<OIN>"
    When the check is searched using an admin token
    Then the created check can be found
    And the check has the organisation "VNG Realisatie B.V." and OIN "00000001821002193000" from the certificate
    And the check has the delegated organisation "<organisation>" and OIN "<OIN>"
    Examples:
      | organisation | OIN     |
      | DelegatedOrg | 81028088288785421250   |
