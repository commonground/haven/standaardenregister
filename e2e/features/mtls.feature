Feature: MTLS
  In order to validate the workings of "standaardenregister"
  As a user of the haven cli
  The flow for users using mtls should be verified

  Scenario: Creating a check using MTLS
    Given a check has been created after a haven cli run using mtls
    When the check is searched using an admin token
    Then the created check can be found
    And the check has the organisation "VNG Realisatie B.V." and OIN "00000001821002193000" from the certificate
