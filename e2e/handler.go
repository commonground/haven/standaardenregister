package e2e

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"math/big"
	"net/http"
	"net/url"
	"time"
)

const (
	JsonContentType = "application/json"
	ChecksEndpoint  = "api/v1/checks"
	TokenEndpoint   = "api/v1/auth/token"
	DELEGATEDHEADER = "X-Delegated-Cert"
)

func Request(u url.URL, body []byte, contentType string, unsafe bool) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	var req *http.Request
	if body != nil {
		req, _ = http.NewRequest(http.MethodPost, u.String(), bytes.NewBuffer(body))
	} else {
		req, _ = http.NewRequest(http.MethodGet, u.String(), nil)
	}

	req.Header.Set("Content-Type", contentType)
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: unsafe,
			},
		},
		Timeout: 10 * time.Second,
	}
	return client.Do(req)
}

func CreateAdminToken(u url.URL, contentType, username, password string, tlsConfig *tls.Config) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	req, _ := http.NewRequest(http.MethodPost, u.String(), nil)

	req.Header.Set("Content-Type", contentType)
	req.SetBasicAuth(username, password)

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
		Timeout: 10 * time.Second,
	}

	return client.Do(req)
}

func PostRequestWithTLS(u url.URL, body []byte, contentType string, tlsConfig *tls.Config) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	req, _ := http.NewRequest(http.MethodPost, u.String(), bytes.NewBuffer(body))
	req.Header.Set("Content-Type", contentType)
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
		Timeout: 10 * time.Second,
	}

	return client.Do(req)
}

func PostRequestWithTLSAndDelegation(u url.URL, body []byte, contentType, encodedCert string, tlsConfig *tls.Config) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	req, _ := http.NewRequest(http.MethodPost, u.String(), bytes.NewBuffer(body))
	req.Header.Set("Content-Type", contentType)
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
		Timeout: 10 * time.Second,
	}

	req.Header.Set(DELEGATEDHEADER, encodedCert)

	return client.Do(req)
}

func GetRequestWithTLS(u url.URL, contentType string, tlsConfig *tls.Config) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	req, _ := http.NewRequest(http.MethodGet, u.String(), nil)
	req.Header.Set("Content-Type", contentType)
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
		Timeout: 10 * time.Second,
	}

	return client.Do(req)
}

func GetRequestWithTLSAndAuth(u url.URL, contentType, token string, tlsConfig *tls.Config) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	req, _ := http.NewRequest(http.MethodGet, u.String(), nil)
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Authorization", "Bearer "+token)
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
		Timeout: 10 * time.Second,
	}

	return client.Do(req)
}

func GenerateTestCertificate(org, serial string) (*tls.Certificate, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	notBefore := time.Now()
	notAfter := notBefore.Add(time.Hour)

	subject := pkix.Name{}

	if org != "" {
		subject.Organization = []string{org}
	}

	if serial != "" {
		subject.SerialNumber = serial
	}

	serialNumber, _ := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject:      subject,
		NotBefore:    notBefore,
		NotAfter:     notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, err
	}

	cert, _ := x509.ParseCertificate(derBytes) // Manually parse the certificate

	tlsCert := tls.Certificate{
		Certificate: [][]byte{derBytes},
		PrivateKey:  priv,
		Leaf:        cert, // Set the Leaf field manually
	}

	return &tlsCert, nil
}
