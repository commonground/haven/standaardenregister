package e2e

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/haven/standaardenregister/e2e/models"
	"net/http"
	"net/url"
)

func (s *StandaardenRegisterFixture) aCheckHasBeenCreatedAfterAHavenCliRunUsingMtls() error {
	content, exists := s.fixtures["cli"]
	if !exists {
		return fmt.Errorf("fixture 'cli' not found")
	}

	u, err := url.JoinPath(s.standaardRegisterRoot, "api/v1/checks")
	if err != nil {
		return err
	}

	parsedURL, err := url.Parse(u)
	if err != nil {
		return err
	}

	mtls := s.createMTLSConfig()

	response, err := PostRequestWithTLS(*parsedURL, content, "", mtls)
	if err != nil || response.StatusCode != http.StatusCreated {
		return err
	}

	var check models.Check
	err = json.NewDecoder(response.Body).Decode(&check)

	s.ctx = context.WithValue(s.ctx, CreatedUUID, check.Uuid)

	return nil
}

func (s *StandaardenRegisterFixture) theCheckIsSearchedUsingAnAdminToken() error {
	u, err := url.JoinPath(s.standaardRegisterRoot, ChecksEndpoint)
	if err != nil {
		return err
	}

	parsedURL, err := url.Parse(u)
	if err != nil {
		return err
	}

	mtls := s.createMTLSConfig()

	tokenPath, err := url.JoinPath(s.standaardRegisterRoot, TokenEndpoint)
	if err != nil {
		return err
	}

	parsedTokenURL, err := url.Parse(tokenPath)
	if err != nil {
		return err
	}

	jwtResponse, err := CreateAdminToken(*parsedTokenURL, "", s.username, s.password, mtls)
	if err != nil {
		return err
	}
	var jwt models.Token
	err = json.NewDecoder(jwtResponse.Body).Decode(&jwt)

	response, err := GetRequestWithTLSAndAuth(*parsedURL, "", *jwt.Token, mtls)
	if err != nil {
		return err
	}

	var checks []models.Check
	err = json.NewDecoder(response.Body).Decode(&checks)

	s.ctx = context.WithValue(s.ctx, AdminResponse, checks)

	return nil
}

func (s *StandaardenRegisterFixture) theCheckHasTheOrganisationAndOINFromTheCertificate(organisation, OIN string) error {
	checks := s.ctx.Value(AdminResponse).([]models.Check)
	createdUUID := s.ctx.Value(CreatedUUID).(uuid.UUID).String()

	for _, check := range checks {
		if check.Uuid.String() == createdUUID {
			if *check.Organisation != organisation {
				return fmt.Errorf("organisation does not match. Expected: %s but was: %s", organisation, *check.Organisation)
			}

			if *check.Oin != OIN {
				return fmt.Errorf("OIN does not match. Expected: %s but was: %s", OIN, *check.Oin)
			}

			break
		}

	}

	return nil
}

func (s *StandaardenRegisterFixture) createMTLSConfig() *tls.Config {
	keyPair, err := tls.X509KeyPair(s.cert, s.key)
	if err != nil {
		return nil
	}

	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM(s.rootCA)

	config := &tls.Config{
		Certificates:       []tls.Certificate{keyPair},
		ClientCAs:          certPool,
		MinVersion:         tls.VersionTLS12,
		InsecureSkipVerify: s.unsafe,
	}

	return config
}
