package e2e

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/haven/standaardenregister/e2e/models"
	"net/http"
	"net/url"
)

const (
	StatusCode = "StatusCode"
)

func (s *StandaardenRegisterFixture) anAnonymousCheckHasBeenCreatedAfterAHavenCliRun() error {
	content, exists := s.fixtures["cli"]
	if !exists {
		return fmt.Errorf("fixture 'cli' not found")
	}

	u, err := url.JoinPath(s.standaardRegisterRoot, ChecksEndpoint)
	if err != nil {
		return err
	}

	parsedURL, err := url.Parse(u)
	if err != nil {
		return err
	}

	response, err := Request(*parsedURL, content, "", s.unsafe)
	if err != nil || response.StatusCode != http.StatusCreated {
		return err
	}

	var check models.Check
	err = json.NewDecoder(response.Body).Decode(&check)

	s.ctx = context.WithValue(s.ctx, CreatedUUID, check.Uuid)

	return nil
}

func (s *StandaardenRegisterFixture) theCheckIsSearchedUsingTheChecksEndpointWithoutMTLS() error {
	u, err := url.JoinPath(s.standaardRegisterRoot, ChecksEndpoint)
	if err != nil {
		return err
	}

	parsedURL, err := url.Parse(u)
	if err != nil {
		return err
	}

	response, err := Request(*parsedURL, nil, "", s.unsafe)
	if err != nil {
		return err
	}

	s.ctx = context.WithValue(s.ctx, StatusCode, response.StatusCode)

	return nil
}

func (s *StandaardenRegisterFixture) theUserGetsABecauseNoMTLSFlowIsEstablished(statusCode int) error {
	statusCodeFromEnv := s.ctx.Value(StatusCode).(int)
	if statusCode != statusCodeFromEnv {
		return fmt.Errorf("codes do not much expected: %v but wasw %v", statusCode, statusCodeFromEnv)
	}

	return nil
}
