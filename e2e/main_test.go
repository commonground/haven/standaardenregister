package e2e

import (
	"embed"
	"fmt"
	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"
	"log"
	"os"
	"strings"
	"testing"
)

var opts = godog.Options{
	Output: colors.Colored(os.Stdout),
	Format: "progress", // can define default values
}

//go:embed features/*.feature
var featureFiles embed.FS

func init() {
	godog.BindCommandLineFlags("godog.", &opts)
}

func InitializeTestSuite(ctx *godog.TestSuiteContext) {
	ctx.BeforeSuite(func() {
		log.Print("running e2e test suite for standaardenregister")
	})
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	e2e, err := New()
	if err != nil {
		log.Print(err.Error())
		os.Exit(1)
	}

	//shared
	ctx.Step(`^the created check can be found$`, e2e.theCreatedCheckCanBeFound)
	ctx.Step(`^the check is searched using an admin token$`, e2e.theCheckIsSearchedUsingAnAdminToken)

	//mtls
	ctx.Step(`^a check has been created after a haven cli run using mtls$`, e2e.aCheckHasBeenCreatedAfterAHavenCliRunUsingMtls)
	ctx.Step(`^the check has the organisation "([^"]*)" and OIN "([^"]*)" from the certificate$`, e2e.theCheckHasTheOrganisationAndOINFromTheCertificate)

	//standard
	ctx.Step(`^an anonymous check has been created after a haven cli run$`, e2e.anAnonymousCheckHasBeenCreatedAfterAHavenCliRun)
	ctx.Step(`^the check is searched using the checks endpoint without MTLS$`, e2e.theCheckIsSearchedUsingTheChecksEndpointWithoutMTLS)
	ctx.Step(`^the user gets a (\d+) because no MTLS flow is established$`, e2e.theUserGetsABecauseNoMTLSFlowIsEstablished)

	//delegated
	ctx.Step(`^a check has been created after a haven cli run using mtls and delegation for organisation "([^"]*)" and OIN  "([^"]*)"$`, e2e.aCheckHasBeenCreatedAfterAHavenCliRunUsingMtlsAndDelegationForOrganisationAndOIN)
	ctx.Step(`^the check has the delegated organisation "([^"]*)" and OIN "([^"]*)"$`, e2e.theCheckHasTheDelegatedOrganisationAndOIN)

}

func TestMain(m *testing.M) {
	format := "pretty"
	var tag string

	for _, arg := range os.Args[1:] {
		if arg == "-test.v=true" {
			format = "progress"
		} else if strings.HasPrefix(arg, "-tags=") {
			tagsString := strings.TrimPrefix(arg, "-tags=")
			tag = strings.Split(tagsString, ",")[0]
		}
	}

	opts := godog.Options{
		Format:          format,
		FeatureContents: getFeatureContents(), // Get the embedded feature files
	}

	if tag != "" {
		opts.Tags = tag
	}

	status := godog.TestSuite{
		Name:                 "godogs",
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options:              &opts,
	}.Run()

	os.Exit(status)
}

func getFeatureContents() []godog.Feature {
	features := []godog.Feature{}
	featureFileNames, _ := featureFiles.ReadDir("features")
	for _, file := range featureFileNames {
		if !file.IsDir() && file.Name() != "README.md" {
			filePath := fmt.Sprintf("features/%s", file.Name())
			fileContent, _ := featureFiles.ReadFile(filePath)
			features = append(features, godog.Feature{Name: file.Name(), Contents: fileContent})
		}
	}
	return features
}
