package e2e

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/haven/standaardenregister/e2e/models"
)

const (
	AdminResponse = "AdminResponse"
	CreatedUUID   = "CreatedUUID"
)

func (s *StandaardenRegisterFixture) theCreatedCheckCanBeFound() error {
	checks := s.ctx.Value(AdminResponse).([]models.Check)
	createdUUID := s.ctx.Value(CreatedUUID).(uuid.UUID).String()

	found := false
	for _, check := range checks {
		if check.Uuid.String() == createdUUID {
			found = true
		}

	}

	if !found {
		return fmt.Errorf("could not find created UUID")
	}

	return nil
}
