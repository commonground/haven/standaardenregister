# Standaardenregister

## Run Locally

### Prerequisites

Before you begin, ensure the following tools are installed and configured on your local machine:

- [Skaffold](https://skaffold.dev/): Automates the build, push, and deploy process for Kubernetes applications.
- [k3d](https://k3d.io/):  Runs lightweight Kubernetes clusters (k3s) inside Containers.
- [kubectl](https://kubernetes.io/docs/reference/kubectl/): A command-line tool for interacting with Kubernetes clusters.

### Getting Started

To set up and run the application locally, follow these steps:

1. **Create a Local Kubernetes Cluster**:
    - Use k3d for local development. You can create a correctly configured cluster with the command below:
      ```bash
      k3d cluster create -c local/k3d-config.yaml
      ```
      
    This usually takes a few minutes to finalize since `cnpgo/cloudnative-pg` needs to be installed with image pulls etc.

2. **Start the Application**:
    - Start the application using Skaffold by running:
      ```bash
      skaffold dev
      ```
    - This deploys your application to the local Kubernetes cluster, with ingress resolving to `https://localhost`.

3. **Test the Application**:
    - Access the API schema to confirm the application is running:
      ```bash
      curl https://localhost/api/v1/schema
      ```

### Accessing the Local Registry

- Self-signed certificates are configured for local development, allowing access to the local registry via:
  ```bash
  curl https://local.standaardenregister.vng/api/v1/schema
  ```

  For this to work, you need to add a reference in your hosts file. You can do this by running the following script:
    
    ```bash
    #!/bin/bash
    
    LINE="127.0.0.1 local.standaardenregister.vng"
    
    if ! grep -qF -- "$LINE" /etc/hosts; then
        echo "$LINE" | sudo tee -a /etc/hosts > /dev/null
    fi
    ```

    This script checks if the required line exists in your /etc/hosts file, and if not, it adds it. This ensures that requests to local.standaardenregister.vng are directed to your local machine.

## Local Development Workflow

This section provides a guide for setting up and running the Standaardenregister (SR) locally in development mode, with live debugging capabilities using `air` for hot reloading and `delve` for remote debugging.


### Steps to Run Locally

1. **Modify `skaffold.yaml`**: To enable debugging, you need to change the docker build target to `develop` in your `skaffold.yaml`. This applies to both the SR and e2e services as shown below:

    ```yaml
    apiVersion: skaffold/v4beta6
    kind: Config
    ...
        target: develop
    ...
    ```

   The `develop` target configures containers to start with [air](https://github.com/cosmtrek/air) for hot reloading and [delve](https://github.com/go-delve/delve) for remote debugging.

2. **Understanding the Development Tools**:

    - **Air**: A live reloading tool for Go applications. It automatically rebuilds and restarts your application upon file changes.
    - **Delve**: A debugger for the Go programming language, allowing you to inspect and control the execution of your Go program.

3. **Start the Development Environment**: Follow the steps outlined in the 'Run Locally' section of this document, with the modifications made to the `skaffold.yaml`.

4. **Port Forwarding**: Once the deployment is up, forward the port to your local machine to attach a debugger:

    ```shell
    kubectl port-forward svc/standaardenregister-svc 2345:2345
    ```

5. **Attach Debugger**: Use Goland's built-in delve debugger or set up a Go remote configuration in your IDE. For other IDEs, refer to the delve [Editor Integration documentation](https://github.com/go-delve/delve/blob/master/Documentation/EditorIntegration.md).

6. **Delve Configuration**: The `.air.toml` file contains the configuration for delve. The key line is:

    ```toml
    full_bin = "dlv --listen=:2345 --headless=true --api-version=2 --accept-multiclient exec /app/delvebuild"
    ```
   
7. **Warning:** When the container starts, the process waits for a debugger to attach before proceeding, which will cause readiness and liveness probes to fail. This makes the service appear as not ready, and port-forwarding may not work until a debugger is attached. Additionally, the build process can be CPU-intensive, so ensure that your deployment has sufficient CPU resources allocated. To accommodate these aspects during debugging, you might need to temporarily remove readiness and liveness probes and adjust resource allocations in your `deployment.yaml`. An example configuration snippet is provided below:
    
    ```yaml
    spec:
      template:
        spec:
          containers:
          - name: your-container-name
            ports:
            - containerPort: 2345
              name: delve
            readinessProbe:
              $patch: delete
            livenessProbe:
              $patch: delete
            resources:
              $patch: delete
    ```

## E2E Testing

End-to-End (E2E) testing is crucial for systems that utilize databases, as it allows testing against an actual database environment rather than relying solely on mocks or stubs. The `standaardenregister` project employs a Behavior-Driven Development (BDD) framework based on Gherkin, implemented in Golang through the [Godog](https://github.com/cucumber/godog) library.

### Creating Tests

Tests in Gherkin follow a structured format, comprising a human-readable `feature` description and a technical implementation. The `feature` should be clear and follow this structure:

```gherkin
Feature: Some feature
  In order to help people write tests
  As a developer
  The README should be complete

Scenario: An example is included
  Given the READMe is made
  When the example is written
  Then the example should be readable
```

You can see the `e2e/features` directory for more examples that have a technical implementation. As the feature files a living documentation good care should be taken to update them regularly!

### Implementing a Test

Consider the following scenario as an example:

```gherkin
  @wip
  Scenario: An example is included
    Given the READMe is made
    When the example is written
    Then the example should be readable
```

To generate skeleton code for undefined steps in your feature file, execute the following command:
```shell
cd e2e
go test ./... -v -tags=@wip
```

This command will output snippets like:

```shell
You can implement step definitions for undefined steps with these snippets:

func theExampleIsWritten() error {
        return godog.ErrPending
}

func theExampleShouldBeReadable() error {
        return godog.ErrPending
}

func theREADMeIsMade() error {
        return godog.ErrPending
}

func InitializeScenario(ctx *godog.ScenarioContext) {
        ctx.Step(`^the example is written$`, theExampleIsWritten)
        ctx.Step(`^the example should be readable$`, theExampleShouldBeReadable)
        ctx.Step(`^the READMe is made$`, theREADMeIsMade)
}
```

Adjust this skeleton code to fit the system structure. Since each test is part of the `e2e` struct, the initialization function should reference methods on this struct:

```shell
func InitializeScenario(ctx *godog.ScenarioContext) {
        ctx.Step(`^the example is written$`, e2e.theExampleIsWritten)
        ctx.Step(`^the example should be readable$`, e2e.theExampleShouldBeReadable)
        ctx.Step(`^the READMe is made$`, e2e.theREADMeIsMade)
}
```

Copy these steps into `main_test.go` in the `InitializeScenario` function. 
Implement the functions as methods of the `StandaardenRegisterFixture` struct:

```shell
func (s *StandaardenRegisterFixture) theExampleIsWritten() error {
        return godog.ErrPending
}

func (s *StandaardenRegisterFixture) theExampleShouldBeReadable() error {
        return godog.ErrPending
}

func (s *StandaardenRegisterFixture) theREADMeIsMade() error {
        return godog.ErrPending
}
```

Add these to the corresponding `*_test.go` file if they relate to an existing feature, or to `shared_test.go` for shared steps.

### Additional Notes

- **Godog Tests Execution**: Godog tests can be executed using the `go test` command. For specific implementation details, refer to `main_test.go`.
- **Feature Embedding**: Features are embedded into the binary, allowing the use of slim images for tests. This approach optimizes test execution and resource usage.
- **Work-in-Progress Tag**: Utilize the `@wip` tag to run or build only scenarios specifically tagged as work-in-progress. This is particularly useful during development or when focusing on specific test scenarios.


## Frontend Dashboard

In order to interact with the standaardenregister a frontend in [HTMX](https://htmx.org/) has been added. This is a WIP and meant more as an extra.

It can be found here:

* local: https://localhost
* review: https://review.standaardenregister.nl
* prod: https://standaardenregister.nl

### Creating Tokens

For fetching checks you need a [JWT](https://jwt.io/) which can be created using the MTLS flow, the same way as creating new checks.

Tokens can be created with the following commands:

#### Non admin tokens:
```shell
 curl -k -X POST "https://localhost/api/v1/auth/token" \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
--cert PKIO.crt \
--key PKIO.key \
```

#### Admin tokens:
```shell
 curl -k -X POST "https://localhost/api/v1/auth/token" \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
--cert PKIO.crt \
--key PKIO.key \
 -u admin:somepassword
```

Admin tokens will fetch all checks while non admin tokens will only fetch checks that match your OIN.
